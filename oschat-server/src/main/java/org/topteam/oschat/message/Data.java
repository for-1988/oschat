package org.topteam.oschat.message;

import java.util.Date;
import java.util.UUID;

import org.topteam.oschat.event.Event;

public class Data<T> {

	

	private Date timestamp = new Date();

	private String type;

	private T data;

	public static Data<TextMessage> buildTextMessage(String msg, String from,
			String to) {
		TextMessage message = new TextMessage();
		message.setId(UUID.randomUUID().toString());
		message.setFrom(from);
		message.setTo(to);
		message.setMsg(msg);
		Data<TextMessage> data = new Data<TextMessage>();
		data.setType("textMsg");
		data.setData(message);
		return data;
	}

	public static Data<Event> buildEventMessage(Event event) {
		Data<Event> data = new Data<Event>();
		data.setType("event");
		data.setData(event);
		return data;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
