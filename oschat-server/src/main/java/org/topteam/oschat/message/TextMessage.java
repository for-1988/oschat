package org.topteam.oschat.message;

public class TextMessage extends Message {
	

	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
