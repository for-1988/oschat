package org.topteam.oschat.event;

import org.topteam.oschat.message.JsonCoder;

/**
 * 所有事件的基类
 * @author JiangFeng
 *
 */
public class Event {
	
	public static class Coder extends JsonCoder<Event> {
	}

	private String id;
	
	private String type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
