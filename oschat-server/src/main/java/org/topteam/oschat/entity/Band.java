package org.topteam.oschat.entity;

import java.util.ArrayList;
import java.util.List;

public class Band {

	private String name;
	
	private int order;
	
	private List<String> users = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}
	
	
}
