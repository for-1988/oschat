package org.topteam.oschat.entity;

import java.util.ArrayList;
import java.util.List;

public class UserRelation {

	public final static String TAG = "relation";

	private String userId;

	private List<Band> bands = new ArrayList<Band>();

	private List<String> groups = new ArrayList<String>();

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	public List<Band> getBands() {
		return bands;
	}

	public void setBands(List<Band> bands) {
		this.bands = bands;
	}
	
}
