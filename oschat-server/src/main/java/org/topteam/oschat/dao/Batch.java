package org.topteam.oschat.dao;

import org.iq80.leveldb.WriteBatch;

public abstract class Batch {

	public abstract void batch(WriteBatch batch);
}
